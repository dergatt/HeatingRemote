//Included Libraries
#include <SPI.h>
#include <PubSubClient.h>
#include <WiFi.h>


//Include Settings
#include "settings.h"

// set Led pin
const int heaterPin = 2;



//Define Pupsub client
  WiFiClient espClient;
  PubSubClient client;

//Set heater on
void heater_on () {
  digitalWrite(heaterPin, LOW); //Turn On heating open circuit
  client.publish(MQTT_STATE, "on", true); //Publish State
}
//Set Heater Off
void heater_off () {
  digitalWrite(heaterPin, HIGH); //Turn off heating close circuit
  client.publish(MQTT_STATE, "off", true); //Publish State
}

//What happens if Message is recieved
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i=0;i<length;i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
  if ((char)payload[0] == '1'){
  heater_off(); 
  }
  if ((char)payload[0] == '0'){
  heater_on();
  }
}




//reconnect MQTT if connection is lost
void reconnect () {
  while (!client.connected()) {
    if (client.connect (device, user, mqtt_password)) {
    Serial.println("Mqtt reconnected");
    client.publish("test/pub","I am here");
    client.subscribe(MQTT_TOPIC);
  } else {
    Serial.println("Conection failed");
    Serial.println(client.state());
    Serial.println("wait");
    delay(5000);
  }
  }
}




//
//Set Up Part
//
//
void setup() {
//Set Up serial
  Serial.begin(115200);  
// Set LED
  pinMode(heaterPin, OUTPUT);
//set up mqtt
  //What is running if message is recieved
  client.setCallback(callback);
  //Set Network Interface
  client.setClient(espClient);
  //Set Mqtt Server
  client.setServer(mqtt_server,1883);

//Wifi
  //Define Wifi
  WiFi.config(ip, gateway, subnet, DNS);
// Connect to Wifi
  WiFi.begin(ssid, password);
while (WiFi.status() != WL_CONNECTED) {
  delay(500);
  Serial.println("Connecting to WiFi..");
}
  Serial.println("Connected to Wifi");
  reconnect();
  delay(500);
  client.publish(MQTT_STATE, "on", true);
  Serial.println("Heating is on");

 delay(500);






}

void loop() {
  //Reconnect to Mqtt
  if (!client.connected()) {
    Serial.println("Connection Lost");
    reconnect();
  }

  client.loop();

}
