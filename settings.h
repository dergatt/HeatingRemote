//Set Variables
//Wifi
const char* ssid = "Wifi Name";
const char* password = "Wifi password";
IPAddress ip(192, 168, 1, 2);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);
IPAddress DNS(192, 168, 1, 1);

//mqqt server
const char* mqtt_server = "192.168.1.3";
const char* device = "MQTT Device Name";
const char* user = "MQTT User";
const char* mqtt_password = "MQTT Password";
//Mqtt topic
const char* MQTT_TOPIC = "heizung/set";
const char* MQTT_STATE = "heizung/state"

